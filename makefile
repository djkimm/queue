CC = gcc
CFLAGS = -g -Wall
TARGET = test.out
OBJECTS = Make_Queue.o Make_Queue_Main.o 

${TARGET} : ${OBJECTS}
	gcc -o ${TARGET} ${OBJECTS}

Make_Queue.o : Make_Queue.c
	 gcc -c Make_Queue.c

Make_Queue_Main.o : Make_Queue_Main.c
	 gcc -c Make_Queue_Main.c
