#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

#define MAX_SIZE 10

/*QUEUE를 이루는 노드 구조체*/
typedef struct qNode{
	int data;
	struct qNode* next;
	struct qNode* prev;
}Node;

/* QUEUE 구조체*/
typedef struct queue_t{
	Node* head;
	Node* tail;
	int count;
}queue_t;

typedef struct qThread{
	queue_t* mQueue;
}qArgPthread;

/*-----------Function---------------*/
queue_t *Init();
void* add_Node(void* aArgPthread);
void* delete_First_Node(queue_t* queue);
void node_Search(queue_t* queue, int number);
int list_Size(queue_t* queue);
void print_List(queue_t* queue);
/*---------------------------------*/

pthread_mutex_t mutex;
pthread_cond_t cond;
#endif
