#ifndef __MAKE_QU_EUE_H__
#define __MAKE_QU_EUE_H__

#define TRUE 1
#define FALSE 0

#include<stdio.h>
#include<stdlib.h>

typedef struct _node_s{
	int data;
	struct _node_s* next;
	struct _node_s* prev;
}Node;

typedef struct queue_t{
	Node* head;
	Node* tail;
	int count;
}queue_t;



queue_t *Init();

void* add_Node(queue_t* queue, void* data);

void* delete_First_Node(queue_t* queue);

void* node_Search(queue_t* queue, int number);

int list_Size(queue_t* queue);

void print_List(queue_t* queue);
#endif
